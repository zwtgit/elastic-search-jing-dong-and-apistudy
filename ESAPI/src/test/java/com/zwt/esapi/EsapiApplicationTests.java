//package com.zwt.esapi;
//
//import org.apache.catalina.User;
//import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
//import org.elasticsearch.action.bulk.BulkRequest;
//import org.elasticsearch.action.bulk.BulkResponse;
//import org.elasticsearch.action.delete.DeleteRequest;
//import org.elasticsearch.action.delete.DeleteResponse;
//import org.elasticsearch.action.get.GetRequest;
//import org.elasticsearch.action.get.GetResponse;
//import org.elasticsearch.action.index.IndexRequest;
//import org.elasticsearch.action.index.IndexResponse;
//import org.elasticsearch.action.search.SearchRequest;
//import org.elasticsearch.action.search.SearchResponse;
//import org.elasticsearch.action.support.master.AcknowledgedResponse;
//import org.elasticsearch.action.update.UpdateRequest;
//import org.elasticsearch.action.update.UpdateResponse;
//import org.elasticsearch.client.RequestOptions;
//import org.elasticsearch.client.RestHighLevelClient;
//import org.elasticsearch.client.indices.CreateIndexRequest;
//import org.elasticsearch.client.indices.CreateIndexResponse;
//import org.elasticsearch.client.indices.GetIndexRequest;
//import org.elasticsearch.cluster.ack.AckedRequest;
//import org.elasticsearch.common.xcontent.XContentType;
//import org.elasticsearch.core.TimeValue;
//import org.elasticsearch.index.query.QueryBuilders;
//import org.elasticsearch.index.query.TermQueryBuilder;
//import org.elasticsearch.search.SearchHit;
//import org.elasticsearch.search.builder.SearchSourceBuilder;
//import org.elasticsearch.search.fetch.subphase.FetchSourceContext;
//import org.json.JSONString;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.boot.test.context.SpringBootTest;
//
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.concurrent.TimeUnit;
//
//@SpringBootTest
//class EsapiApplicationTests {
//
//    private static final JSONString JSON = null;
//    @Autowired
//    @Qualifier("restHighLevelClient")   //不加也行 但定义的名字要是这个名字
//    private RestHighLevelClient client;
//
//    //测试索引的创建 Request PUT lijiatu_index
//    @Test
//    void testCreateIndex() throws IOException {
//        //1. 创建索引请求
//        CreateIndexRequest request = new CreateIndexRequest("lijiatu_index");
//        //2. 客户端执行请求
//        CreateIndexResponse createIndexRequest = client.indices().create(request, RequestOptions.DEFAULT);
//
//        System.out.println(createIndexRequest);
//    }
//
//    //测试获取索引,判断其是否存在
//    @Test
//    void testExistIndex() throws IOException {
//        GetIndexRequest request = new GetIndexRequest("lijiatu_index");
//        boolean exists = client.indices().exists(request, RequestOptions.DEFAULT);
//        System.out.println(exists);
//    }
//
//    //测试删除索引
//    @Test
//    void testDeleteIndex() throws IOException {
//        DeleteIndexRequest request = new DeleteIndexRequest("lijiatu_index");
//        //删除
//        AcknowledgedResponse delete = client.indices().delete(request, RequestOptions.DEFAULT);
//        System.out.println(delete.isAcknowledged());
//    }
//
//    //=====================================================================================
//
//    //测试添加文档
//    @Test
//    void testAddDocument() throws IOException {
//        //创建对象
//        User user = new User("李嘉图", 12);
//        //创建请求
//        IndexRequest request = new IndexRequest("ahui_index");
//
//        //规则 put lijiatu_index/_doc/1
//        request.id("1");
//        request.timeout(TimeValue.timeValueSeconds(1));
//        request.timeout("1s");
//
//        //将我们的数据放入请求 json (使用fastjson进行转换)
//        IndexRequest source = request.source(JSON.toJSONString(user), XContentType.JSON);
//
//        //客户端发送请求, 获取响应的结果
//        IndexResponse indexResponse = client.index(request, RequestOptions.DEFAULT);
//
//        //返回具体的json信息
//        System.out.println(indexResponse.toString());
//        //对应我们命令返回的状态 CREATED
//        System.out.println(indexResponse.status());
//
//    }
//
//
//    //获取文档
//    @Test
//    void testIsExistes() throws IOException {
//        GetRequest getRequest = new GetRequest("lijiatu_index", "1");
//        //不获取返回的_source 的上下文了
//        getRequest.fetchSourceContext(new FetchSourceContext(false));
//        getRequest.storedFields("_none_");
//
//        boolean exists = client.exists(getRequest, RequestOptions.DEFAULT);
//        System.out.println(exists);
//
//    }
//
//
//    //获取文档的信息
//    @Test
//    void testGetDocument() throws IOException {
//        GetRequest getRequest = new GetRequest("lijiatu_index", "1");
//        GetResponse getResponse = client.get(getRequest, RequestOptions.DEFAULT);
//        //打印文档的内容
//        System.out.println(getResponse.getSourceAsString());
//        //返回的全部内容和命令是一样的
//        System.out.println(getResponse);
//
//    }
//
//
//    //获取文档的信息
//    @Test
//    void testUpdateRequest() throws IOException {
//        UpdateRequest updateRequest = new UpdateRequest("lijiatu_index", "1");
//        updateRequest.timeout("1s");
//
//        User user = new User("李嘉图学Java", 22);
//        updateRequest.doc(JSON.toJSONString(user), XContentType.JSON);
//
//        UpdateResponse updateResponse = client.update(updateRequest, RequestOptions.DEFAULT);
//        System.out.println(updateResponse.status());
//
//    }
//
//
//    //删除文档记录
//    @Test
//    void testDeleteRequest() throws IOException {
//        DeleteRequest deleteRequest = new DeleteRequest("lijiatu_index", "1");
//        deleteRequest.timeout("1s");
//
//        DeleteResponse delete = client.delete(deleteRequest, RequestOptions.DEFAULT);
//        System.out.println(delete.status());
//
//    }
//
//    //特殊的，实际项目中一般都会批量插入数据
//    @Test
//    void testBulkRequest() throws IOException {
//        BulkRequest bulkRequest = new BulkRequest();
//        bulkRequest.timeout("10s");
//
//        ArrayList<User> userList = new ArrayList<>();
//        userList.add(new User("lijiatu1", 18));
//        userList.add(new User("lijiatu1", 18));
//        userList.add(new User("lijiatu1", 18));
//        userList.add(new User("lijiatu1", 18));
//        userList.add(new User("lijiatu1", 18));
//        userList.add(new User("lijiatu1", 18));
//
//        //批量处理请求
//        for (int i = 0; i < userList.size(); i++) {
//            //批量更新和批量修改等, 就在这里修改对应的请求就可以了
//            bulkRequest.add(new IndexRequest("ahui_index")
//                    .id("" + (i + 1)) //不加id的话会默认生成随机id
//                    .source(JSON.toJSONString(userList.get(i)), XContentType.JSON));
//        }
//        BulkResponse bulkResponse = client.bulk(bulkRequest, RequestOptions.DEFAULT);
//        //是够失败, 返回false代表成功
//        System.out.println(bulkResponse.hasFailures());
//    }
//
//
//    //查询
//    //SearchRequest 搜索请求
//    //SearchSourceBuilder 条件构造
//    //HighLightBuilder 构建高亮
//    //xxx QueryBuilder 对应我们刚才看到的所有命令
//    @Test
//    void testSearch() throws IOException {
//        SearchRequest searchRequest = new SearchRequest("lijiatu_index");
//        //构建搜索条件
//        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
//
//        //查询条件,我们可以使用QueryBuilders 工具类来实现
//        //QueryBuilders.termQuery  精确匹配
//        //QueryBuilders.matchAllQuery 匹配所有
//        TermQueryBuilder termQueryBuilder = QueryBuilders.termQuery("name", "lijiatu");
////        MatchAllQueryBuilder matchAllQueryBuilder = QueryBuilders.matchAllQuery();
//        sourceBuilder.query(termQueryBuilder);
//        sourceBuilder.timeout(new TimeValue(60, TimeUnit.SECONDS));
//
//        searchRequest.source(sourceBuilder);
//
//        SearchResponse searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);
//        System.out.println(JSON.toJSONString(searchResponse.getHits()));
//        System.out.println("==========================================");
//        for (SearchHit documentFileds : searchResponse.getHits().getHits()) {
//            System.out.println(documentFileds.getSourceAsMap());
//        }
//    }
//
//
//}
